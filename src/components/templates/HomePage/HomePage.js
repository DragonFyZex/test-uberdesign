import React from 'react'

import Header from 'components/organisms/Header/Header';
import Head from 'components/organisms/Head/Head';
import MapButtonCombo from 'components/molecules/MapButtonCombo/MapButtonCombo';
import Articles from 'components/organisms/Articles/Articles';
import Phone from 'components/atoms/Phone/Phone';
import LoadingBox from 'components/atoms/LoadingBox/LoadingBox';
import './HomePage.css'

export default () => (
    <div className = "homepage">
        <Header />
        <Head />
        <MapButtonCombo />
        <Articles />
        <Phone />
        <Phone />
        <LoadingBox />
    </div>
)