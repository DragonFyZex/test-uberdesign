import React from 'react'
import Article from 'components/atoms/Article/Article';


export default () => (
    // This is for how much space the articles can expand into, width
    <div style={{marginLeft: '13.2vw', display: 'flex', flexDirection: 'row', width: 'calc(37.5% * 2 + 2vw)', height: '10%', justifyContent: 'initial'}}>
        <Article color={"#f3f4f4"} />
        <Article color={"#2c2c2d"} />
    </div>
)