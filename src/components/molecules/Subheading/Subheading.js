import React from 'react'

import './Subheading.css'
import HeadlineLink from 'components/atoms/HeadlineLink/HeadlineLink';
import ScrollDownArrow from 'components/atoms/ScrollDownArrow/ScrollDownArrow';

export default () => (
    <div className = "col-sm-12 subheading">
        <HeadlineLink />
        <p style={{fontFamily: 'Muli', fontSize: "0.8rem", color: 'transparent', userSelect: 'none', MozUserSelect: 'none'}}>
            This is so that the animation of the text won't push down all the other content
        </p>
        <ScrollDownArrow />
    </div>
)