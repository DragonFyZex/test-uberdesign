import React from 'react'
import UberLogo from 'components/atoms/UberLogo/UberLogo'
import UberSlogan from 'components/atoms/UberSlogan/UberSlogan'
import './LogoCombo.css'

export default ({text}) => (
    <div className = "logocombo col-sm-6">
        <UberLogo />
        <UberSlogan />
    </div>
)