import React from 'react'
import Map from 'components/atoms/Map/Map';
import MapArrow from 'components/atoms/MapArrow/MapArrow';
import './MapButtonCombo.css'


export default ({text}) => (
    <div className = "col-md-9 mb-4 mapbuttoncombo">
        <Map>
            <MapArrow />
        </Map>
    </div>
)