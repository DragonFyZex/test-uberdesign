import React from 'react'
import HeaderLink from 'components/atoms/HeaderLink/HeaderLink'
import './Navigation.css'

export default () => (
    <div className = "navigationContainer col-sm-6"> 
        <HeaderLink text = {"Casestudies"} />
        <HeaderLink text = {"Events"} />
        <HeaderLink text = {"Join Us"} />
    </div>
)