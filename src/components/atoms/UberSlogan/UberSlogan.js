import React from 'react'
import './UberSlogan.css'

export default () => (
    <p className = "uberslogan">The Craft Behind our Design</p>
)