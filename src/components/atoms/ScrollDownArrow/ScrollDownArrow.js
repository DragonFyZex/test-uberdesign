import React from 'react'
import './ScrollDownArrow.css'

export default () => (
    <p className = "scrolldownarrow mb-0">↓</p>
)