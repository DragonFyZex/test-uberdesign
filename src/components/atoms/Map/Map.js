import React from 'react'
import './Map.css'

export default ({children}) => (
    <div className = "map">
        {children}
    </div>
)