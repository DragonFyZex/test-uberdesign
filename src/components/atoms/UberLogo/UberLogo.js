import React from 'react'
import './UberLogo.css'

export default () => (
    <p className = "uberlogo mb-0">UBER</p>
)