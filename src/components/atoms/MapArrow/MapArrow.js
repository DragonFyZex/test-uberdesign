import React from 'react'
import './MapArrow.css'

export default () => (
    <div className = "maparrow mb-4">
        <img src={require('static/img/rightarrow.png')} className="arrowimg" />
    </div>
)