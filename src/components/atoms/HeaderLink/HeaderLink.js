import React from 'react'
import './HeaderLink.css'

export default ({text}) => (
    <p className = "headerlink mr-3">{text}</p>
)