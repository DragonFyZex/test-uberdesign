import React from 'react'
import './Headline.css'

export default ({text}) => (
    <p className = "headline col-sm-3 mb-2">{text}</p>
)