import React from 'react'
import './ArticleContainer.css'

export default ({children}) => (
    <div className = "articlecontainer">
        {children}
    </div>
)