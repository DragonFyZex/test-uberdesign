import React from 'react'
import './Article.css'

export default ({color}) => (
    // We need to make a surrounding div to get around the space-between, space-between would make the article on the right animation from right to left 
    <div className = "articlewrapper">
        <div className = "article" style={{backgroundColor: color}} />
    </div>
)