import React from 'react'

import LogoCombo from 'components/molecules/LogoCombo/LogoCombo';
import Navigation from 'components/molecules/Navigation/Navigation';
import './Header.css'

export default () => (
    <div className = 'header col-sm-9 mt-4'>
        <LogoCombo />
        <Navigation />
    </div>
)