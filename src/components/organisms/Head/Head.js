import React from 'react'

import './Head.css'
import HeadlineLink from 'components/atoms/HeadlineLink/HeadlineLink';
import ScrollDownArrow from 'components/atoms/ScrollDownArrow/ScrollDownArrow';
import Headline from 'components/atoms/Headline/Headline';
import Subheading from 'components/molecules/Subheading/Subheading';

export default () => (
    <div className = "head col-sm-9">
        {/* Split up into two so they would render their animation seperately */}
        <Headline text = {"The Newest"}/>
        <Headline text = {"Uber App"} />
        <Subheading />
    </div>
)