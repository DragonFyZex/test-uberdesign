import React from 'react'
import ArticleContainer from 'components/atoms/ArticleContainer/ArticleContainer'
import ArticleList from 'components/molecules/ArticleList/ArticleList';
import './Articles.css'


export default () => (
    <div className = "articles">
        <ArticleContainer>
               <ArticleList />
        </ArticleContainer>
    </div>
)